module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

		globalConfig: {
			development_html: 				'development/layout',
			development_css: 				'development/styles',
			development_js: 				'development/scripts',
			development_img: 				'images',
			production_html: 				'production/layout',
			production_css: 				'production/styles',
			production_js: 					'production/scripts',
			production_img: 				'production'
		},

		clean: [
			"<%= globalConfig.development_css %>/tmp/*.css",
			"<%= globalConfig.development_js %>/tmp/*.js",

		],
		
		jshint: {
			all: [
				'<%= globalConfig.development_js %>/*.js',
				'Gruntfile.js'
			]
		},

		htmlmin: {
			dist: {
				options: {
					removeComments: true,
					collapseWhitespace: true
				},
				files: {
					'<%= globalConfig.production_html %>/includes/header-tag.html':
					'<%= globalConfig.development_html %>/includes/header-tag.html',

					'index.html': 
					'<%= globalConfig.development_html %>/main-layout/index.html',
					
					'parx-casino.html': 
					'<%= globalConfig.development_html %>/main-layout/parx-casino.html',

					'empire-city-casino.html': 
					'<%= globalConfig.development_html %>/main-layout/empire-city-casino.html',

					'comm-unique.html': 
					'<%= globalConfig.development_html %>/main-layout/comm-unique.html',

					'le-voyaguer.html': 
					'<%= globalConfig.development_html %>/main-layout/le-voyaguer.html',

					'isight.html': 
					'<%= globalConfig.development_html %>/main-layout/isight.html',

					'moneygaming.html': 
					'<%= globalConfig.development_html %>/main-layout/moneygaming.html',

					'enracha-bingo.html': 
					'<%= globalConfig.development_html %>/main-layout/enracha-bingo.html',

					'norsk-tipping.html': 
					'<%= globalConfig.development_html %>/main-layout/norsk-tipping.html',

					'gameaccount-network.html': 
					'<%= globalConfig.development_html %>/main-layout/gameaccount-network.html',

					'various-logos.html': 
					'<%= globalConfig.development_html %>/main-layout/various-logos.html'

					// '<%= globalConfig.production_html %>/tmp/Signoff.html': 
					// '<%= globalConfig.development_html %>/main-layout/sign-off.html',

					// '<%= globalConfig.production_html %>/tmp/Attendance.html': 
					// '<%= globalConfig.development_html %>/main-layout/attendance.html',
				}
			}
		},

		concat: {
			options: {
				sourceMap: true
			},
			dist: {
				src: [
					'<%= globalConfig.development_js %>/libs/angular.min.js',
					'<%= globalConfig.development_js %>/libs/*.js',
					'<%= globalConfig.development_js %>/main.js'
				],
				dest: '<%= globalConfig.development_js %>/tmp/production.tmp.js'
			}
		},
		
		uglify: {
			options: {
				sourceMap: true,
				sourceMapIncludeSources: true,
				sourceMapIn: '<%= globalConfig.development_js %>/tmp/production.tmp.js.map'
			},
			build: {
				src: '<%= globalConfig.development_js %>/tmp/production.tmp.js',
				dest: '<%= globalConfig.production_js %>/production.min.js'
			}
		},

		imagemin: {
			dynamic: {
				options: {
					optimizationLevel: 1
				},
				files: [{
					expand: true,
					src: [
						'<%= globalConfig.development_img %>/*.{png,jpg,gif}',
						'<%= globalConfig.development_img %>/**/*.{png,jpg,gif}',
						'<%= globalConfig.development_img %>/**/**/*.{png,jpg,gif}',
					],
					dest: '<%= globalConfig.production_img %>/'
				}]
			}
		},

		sass: {
			dist: {
				options: {
					style: 'compressed'
				},
				files: {
					'<%= globalConfig.development_css %>/tmp/master.tmp.css':
					'<%= globalConfig.development_css %>/master.scss',
				}
			} 
		},

		concat_css: {
			files: {
				src: [
					'<%= globalConfig.development_css %>/libs/reset.css',
					'<%= globalConfig.development_css %>/libs/*.css',
					'<%= globalConfig.development_css %>/tmp/master.tmp.css'
				],
				dest: "<%= globalConfig.development_css %>/tmp/main.tmp.css"
			}
		},
		
		cssmin: {
		  combine: {
			files: {
			  '<%= globalConfig.production_css %>/main.min.css': ['<%= globalConfig.development_css %>/tmp/main.tmp.css']
			}
		  }
		},



		watch: {
			options: {
				livereload: true
			},

			html: {
				files: [
					'<%= globalConfig.development_html %>/*.html',
					'<%= globalConfig.development_html %>/**/*.html',
					'<%= globalConfig.development_html %>/**/**/*.html'
				],
				tasks: [
					'htmlmin'
				]
			},

			scripts: {
				files: [
					'<%= globalConfig.development_js %>/**/**/*.js',
					'<%= globalConfig.development_js %>/**/*.js',
					'<%= globalConfig.development_js %>/*.js'
				],
				tasks: [
					'jshint',
					'concat',
					'uglify'
				],
				options: {
					spawn: false,
				},
			},

			image: {
				files: [
					'<%= globalConfig.development_img %>/*.{png,jpg,gif}',
					'<%= globalConfig.development_img %>/**/*.{png,jpg,gif}',
					'<%= globalConfig.development_img %>/**/**/*.{png,jpg,gif}',

					'<%= globalConfig.development_img %>/*.png',
					'<%= globalConfig.development_img %>/**/*.png',
					'<%= globalConfig.development_img %>/**/**/*.png',

					'<%= globalConfig.development_img %>/*.jpg',
					'<%= globalConfig.development_img %>/**/*.jpg',
					'<%= globalConfig.development_img %>/**/**/*.jpg',

					'<%= globalConfig.development_img %>/*.gif',
					'<%= globalConfig.development_img %>/**/*.gif',
					'<%= globalConfig.development_img %>/**/**/*.gif'
				],
				tasks: [
					'newer:imagemin:dynamic'
				],
			},
			
			sass: {
				files: [
					'<%= globalConfig.development_css %>/**/*.scss',
					'<%= globalConfig.development_css %>/*.scss',
					'<%= globalConfig.development_css %>/libs/*.css'
				],
				tasks: [
					'sass',
					'concat_css',
					'cssmin'
				],
				options: {
					spawn: false,
				},
			},

		}


	});


	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-concat-css');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-newer');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-watch');


	grunt.registerTask('default', [
		'clean',
		'jshint',
		'htmlmin',
		'concat',
		'uglify',
		'newer:imagemin:dynamic',
		'sass',
		'concat_css',
		'cssmin',
		'watch'
	]);

};